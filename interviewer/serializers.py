from django.contrib.auth.models import User, Group
from rest_framework import serializers

import models

class InterviewerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Interviewer
        fields = ('id','name', 'email')

class InterviewPackSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.InterviewPack
        fields = ('id','interviewer', 'villagersurveysets')
