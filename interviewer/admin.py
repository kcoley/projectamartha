from django.contrib import admin

# Register your models here.
from .models import Interviewer, InterviewPack


admin.site.register(Interviewer)
admin.site.register(InterviewPack)
