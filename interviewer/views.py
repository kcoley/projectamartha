from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

import models
from rest_framework import viewsets

import django_filters
import serializers
# Create your views here.

class InterviewerSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Interviewer.objects.all()
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('name','email',)
    serializer_class = serializers.InterviewerSerializer

class InterviewPackSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = models.InterviewPack.objects.all()
    serializer_class = serializers.InterviewPackSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('interviewer','villagersurveysets', 'interviewer__name', )
