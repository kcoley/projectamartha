from __future__ import unicode_literals

from django.db import models

import survey.models
import interviewer.models


# Create your models here.

class Interviewer(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()

    def __unicode__(self):
        return u'%s' % self.name

class InterviewPack(models.Model):
    interviewer = models.ForeignKey("interviewer.Interviewer")
    villagersurveysets = models.ManyToManyField("survey.VillagerSurveySets")

    def __unicode__(self):
        return u'Interviewer Pack for %s' % self.interviewer.name
