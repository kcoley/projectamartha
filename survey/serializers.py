from django.contrib.auth.models import User, Group
from rest_framework import serializers

from models import SurveySet, GenericAnswer, Choice, Question, Villager, SurveyResponse, Answer
import models

class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        #import six
        import binascii
        import uuid

        is_base64 = False

        # Check if this is a base64 string
        try:
            base64.decodestring(data)
            is_base64 = True
        except binascii.Error:
            is_base64 = False

        if is_base64:
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class SurveySetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SurveySet
        fields = ('id','survey_title', 'survey_description', 'answer_set')

class GenericAnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GenericAnswer
        fields = ('id','question', 'boolean_response', 'text_response', 'choices')

class AnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Answer
        fields = ('id','question', 'boolean_answer', 'text_response', 'choices')

class ChoiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Choice
        fields = ('id','choice',)

class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question
        fields = ('id', 'question',)

class VillagerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Villager
        fields = ('id','name', 'location')


class SurveyResponseSerializer(serializers.HyperlinkedModelSerializer):
    villager_image = Base64ImageField(
        max_length=None, use_url=True,
    )
    class Meta:
        model = SurveyResponse
        fields = ('id','timestamp', 'villager','interviewer', 'answers', 'surveyset', 'villager_image' )

class VillagerSurveySetsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.VillagerSurveySets
        fields = ('id','villager', 'surveysets')
