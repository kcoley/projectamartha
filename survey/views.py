from django.shortcuts import render

from django.contrib.auth.models import User, Group
from models import SurveySet, GenericAnswer, Choice, Question, Villager, SurveyResponse, Answer, VillagerSurveySets
from rest_framework import viewsets
from serializers import UserSerializer, GroupSerializer, SurveySetSerializer, GenericAnswerSerializer, ChoiceSerializer, QuestionSerializer, VillagerSerializer, SurveyResponseSerializer, AnswerSerializer, VillagerSurveySetsSerializer

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class SurveySetViewSet(viewsets.ModelViewSet):
    '''
    '''
    queryset = SurveySet.objects.all()
    serializer_class = SurveySetSerializer

class GenericAnswerViewSet(viewsets.ModelViewSet):
    '''
    '''
    queryset = GenericAnswer.objects.all()
    serializer_class = GenericAnswerSerializer

class ChoiceViewSet(viewsets.ModelViewSet):
    '''
    '''
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer

class QuestionViewSet(viewsets.ModelViewSet):
    '''
    '''
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

class VillagerViewSet(viewsets.ModelViewSet):
    '''
    '''
    queryset = Villager.objects.all()
    serializer_class = VillagerSerializer

class SurveyResponseViewSet(viewsets.ModelViewSet):
    '''
    '''
    queryset = SurveyResponse.objects.all()
    serializer_class = SurveyResponseSerializer

class AnswerViewSet(viewsets.ModelViewSet):
    '''
    '''
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

class VillagerSurveySetsSet(viewsets.ModelViewSet):
    '''
    '''
    queryset = VillagerSurveySets.objects.all()
    serializer_class = VillagerSurveySetsSerializer
