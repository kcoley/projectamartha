from __future__ import unicode_literals

from django.db import models
import interviewer.models

# Create your models here.

class Question(models.Model):
    question = models.CharField(max_length=255)

    def __unicode__(self):
        return u'%s' % self.question

class Choice(models.Model):
    choice = models.CharField(max_length=255)

    def __unicode__(self):
        return u'%s' % self.choice

class GenericAnswer(models.Model):
    question = models.ForeignKey("Question")

    boolean_response = models.BooleanField(blank=True)
    text_response = models.TextField(blank=True)
    choices = models.ManyToManyField(Choice, blank=True)

    def __unicode__(self):
        return u'Answer to "%s"' % self.question.question

class SurveySet(models.Model):
    survey_title = models.CharField(max_length=255)
    survey_description = models.TextField()
    answer_set = models.ManyToManyField(GenericAnswer)

    def __unicode__(self):
        return self.survey_title

    class Meta:
        ordering = ('survey_title',)

class Villager(models.Model):
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering=('name',)

class Answer(models.Model):
    question = models.ForeignKey("GenericAnswer")
    boolean_answer = models.BooleanField(blank=True)
    text_response = models.TextField(blank=True)
    choices = models.ManyToManyField(Choice, blank=True)

    def __unicode__(self):

        if self.boolean_answer:
            return u'"%s"' % (self.question.question.question + u': True')
        elif self.text_response:
            return u'"%s"' % (self.question.question.question + u': ' + self.text_response)
        else:
            message = "["
            for c in self.choices.all():
                message += c.choice + ";"
            message += "]"
            return u'"%s"' % (self.question.question.question + u': ' + message)


class SurveyResponse(models.Model):
    villager = models.ForeignKey("Villager")
    interviewer = models.ForeignKey("interviewer.Interviewer")
    answers = models.ManyToManyField("Answer")
    timestamp = models.DateTimeField()
    surveyset = models.ForeignKey("SurveySet")
    villager_image = models.ImageField(upload_to='villager_images')

    def __unicode__(self):
        return u'SurveyResponse "%s\%s"' % (self.villager.name, self.surveyset.survey_title)

class VillagerSurveySets(models.Model):
    villager = models.ForeignKey("Villager")
    surveysets = models.ManyToManyField("SurveySet")
    def __unicode__(self):
        return u'VillagerSurveySets "%s"' % (self.villager.name)
