from django.contrib import admin

# Register your models here.
from .models import Question, Choice, GenericAnswer, SurveySet, Villager, SurveyResponse, Answer, VillagerSurveySets
#import .models


admin.site.register(Question)
admin.site.register(Choice)
admin.site.register(GenericAnswer)
admin.site.register(SurveySet)
admin.site.register(Villager)
admin.site.register(SurveyResponse)
admin.site.register(Answer)
admin.site.register(VillagerSurveySets)
