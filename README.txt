Run these commands to get the docker images set up

docker-compose run web python manage.py makemigrations
docker-compose run web python manage.py migrate --run-syncdb
docker-compose run web python manage.py createsuperuser (sets up username and password)
docker-compose up

Once the site is setup, go to localhost:8000/admin to get to the admin page
Log in with the username and password you set up

From the admin page, you should be able to see all the available tables
